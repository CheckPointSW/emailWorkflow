# emailWorkflow

- Script1 receives request and data from Service now  
(For simplicity reads from snow_in.ini)
- Script1 prepares a Firewall Control Change Request
- Script1 sends FWC request to FW Team
- FW Team validates and cleans preapred FWC request
- FW Team forwards FWC request to Network Team
- Also IT Security team is involved in the thread
- IT Security team approves the FWC request and forwards to Script2 mailbox  
(For simplicity FW Team is the only team in this workflow)
- Script2 receives approved FWC request
- Script2 deploys all changes (objects, rules)
- Script2 doesn't publish (only set `continue-session-in-smartconsole`) and informs FW Team
- FW Team reviews proposed changes in SmartConsole and publishes/deploys


# Preparation

## emails

`keyring set emailWorkflow cpauto_pass`


## Libs

pip install git+https://github.com/CheckPointSW/cp_mgmt_api_python_sdk  
https://fedingo.com/how-to-send-html-mail-with-attachment-using-python/

## ini files

### emailWorkflow.ini

#### Sections

**[emails]**
- server settings
- mailboxes
  * snow: ServiceNow (not used now)
  * fwteam: Humans, may forward to Network and IT Security teams...
  * cpauto: 
    - Read `snow_in.ini` and sends FWC request to fwteam
    - Read FWC reuest email (after approval) and deploy (`continue-session-in-smartconsole` instead of `publish`)
    - Inform fwteam about deployment

### snow_in.html

HTML for FWC request jinja2 templated

#### Tables

- tbl0, tbl1, tbl2: General information (Request No, justification, etc)
- 3.1.1 Define Objects/Groups used in rules
- 3.1.2 Modify Object Membership in Group
- 3.1.3 Firewall Policies - Access Rules
- 3.1.4 Firewall Translation Rules

# Modules

## General Note

Login to CMA (DMN) and store session for fatser switching.  
Also stores if any change were done (need to validate and publish).  
global variable `LoggedDMN[DMN_name] = { "client": mdm_client_dmn, "session": session_uid, "changed": False }`

## emailWorkflow.py

Read input from ServiceNow


## emailWorkflow-deploy.py

Receive FWC request from cpauto@ and deploys it.  
- Check mailbox
- Take "unseen" messages
- 

### deploy311( cp_table ):
> Define Objects/Groups used in rules

### deploy312( cp_table ):
> Modify Object Membership in Group

### deploy313( cp_table ):
> Firewall Policies - Access Rules

### deploy( cp_tables ):
> Deploy objects, rules, etc from FWC sheet tables


# Questions

