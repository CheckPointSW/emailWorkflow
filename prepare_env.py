# pylint: disable=W1203,C0116,C0410
"""Fill objects and rules to email from user imput"""

import sys
import os
import logging

import configparser
import keyring

from jinja2 import Template
from regex import E

import cpa  # Functions for Check Point Mgmt API

# from query_temp import q_zone_by_host as q_zone_by_host
# from query_temp import q_cma_by_zone as q_cma_by_zone
from query_temp import prepare_gateways as prepare_gateways
from query_temp import prepare_packages as prepare_packages


cfg = None
sectEmails = "EMAILS"
sectCP = "CP"
sectCMA = "CP_CMA_IP"
LogLevel = logging.INFO
# LogLevel = logging.DEBUG

def init():
    logging.basicConfig(format='%(levelname)s: %(message)s', stream=sys.stdout, level=LogLevel) # DEBUG INFO WARNING ERROR CRITICAL
    global cfg
    cfg = configparser.ConfigParser(inline_comment_prefixes="#")
    cfg.optionxform = lambda option: option # Make case sensitive
    cfg.read("emailWorkflow.ini")


def prep_gateways():
    cpa.logfuncname()
    for gw in prepare_gateways:
        dmn = gw["CMA"]
        (mdm_client_dmn, res) = cpa.login_mdm(
            cfg[sectCP]["MDS_IP"],
            os.getenv( cfg[sectCP]["MDS_ENV_USERNAME"] ),
            os.getenv( cfg[sectCP]["MDS_ENV_PASSWORD"] ),
            dmn = dmn
        )
        del gw["CMA"]
        gw["ignore-warnings"] = True
        response = mdm_client_dmn.api_call("add-simple-gateway", gw)
        logging.debug(f"{dmn}: add-simple-gateway \n{response.res_obj}\n")
        if response.success:
            logging.info(f"{dmn}: add-simple-gateway {gw['name']} OK\n")
        mdm_client_dmn.api_call("publish")
        mdm_client_dmn.api_call("logout")
    return # prep_gateways


def prep_packages():
    cpa.logfuncname()
    for pkg in prepare_packages:
        dmn = pkg["CMA"]
        (mdm_client_dmn, res) = cpa.login_mdm(
            cfg[sectCP]["MDS_IP"],
            os.getenv( cfg[sectCP]["MDS_ENV_USERNAME"] ),
            os.getenv( cfg[sectCP]["MDS_ENV_PASSWORD"] ),
            dmn = dmn
        )
        del pkg["CMA"]
        pkg["ignore-warnings"] = True
        response = mdm_client_dmn.api_call("add-package", pkg)
        logging.debug(f"{dmn}: add-package \n{response.res_obj}\n")
        if response.success:
            logging.info(f"{dmn}: add-simple-gateway {pkg['name']} OK\n")
        mdm_client_dmn.api_call("publish")
        mdm_client_dmn.api_call("logout")
    return # prep_packages


def main():
    # prep_gateways()
    # prep_packages()
    # show_pkg_gw("Internet-001", "gwI1a")
    return # main

if __name__ == "__main__":
    # execute only if run as a script
    init()
    main()
