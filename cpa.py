# pylint: disable=W1203,C0116,C0410
"""Functions for Check Point Mgmt API"""

from time import sleep
import logging, inspect, sys, colorlog
from cpapi import APIClient, APIClientArgs, APIResponse


default_color = "Crete Blue" # for new objects if not specified
# [aquamarine, black, blue, crete blue, burlywood, cyan, dark green, khaki, orchid, dark orange, dark sea green, pink, turquoise, dark blue, firebrick, brown, forest green, gold, dark gold, gray, dark gray, light green, lemon chiffon, coral, sea green, sky blue, magenta, purple, slate blue, violet red, navy blue, olive, orange, red, sienna, yellow]

def logfuncname( extra="" ):
    """Prints function name with ``extra``(str) parameters to logging.debug"""
    logging.debug( f" -> {inspect.stack()[1][3]}({extra}):" )
    # logging.info( f" -> {inspect.stack()[1][3]}({extra}):" )


def print_note(str, err=False):
    """ Print notes with color. Red (error) if err=True """
    # https://github.com/borntyping/python-colorlog/blob/main/colorlog/escape_codes.py
    reset = "\x1b[0m"
    grey = "\x1b[38;21m"
    yellow = "\x1b[33;21m"
    red = "\x1b[31;21m"
    cyan = "\x1b[36;21m"
    bold_red = "\x1b[31;1m"
    light_white = "\x1b[97;1m"

    if err:
        fg = bold_red
    else:
        fg = light_white

    print(fg+str+reset)
    return  # print_note


def validate_ip(str):
    """ Validates if the string is an IP address """
    octets = str.split('.')
    if len(octets) != 4:
        return False
    for octet in octets:
        if not octet.isdigit():
            return False
        octet_int = int(octet)
        if octet_int < 0 or octet_int > 255:
            return False
    return True # validate_ip


LoggedDMN = {}
""" [(mds_ip, mds_username, dmn)] = { "client": APIClient, "session": session_uid, "changed": False } """

def setLoggedDMN(key, param : str, val):
    """ Changes a parameter in LoggedDMN if needed

    Args:
        key (tuple): (MDS_ip, MDS_username, dmn)
        param (str): fro example, "changed"
        val (_type_): according to parameter

    Returns:
        bool: True - changed, False - not changed (no need)
    """

    global LoggedDMN
    if not LoggedDMN[key][param]:
        LoggedDMN[key][param] = val
        return True
    return False # setLoggedDMN


def find_host(client, host_str, auto_create_prefix=""):
    """Return a list of object names if exist

    Args:
        client (APICli):
        host_str (str): object name or IP
        auto_create_prefix (str):
            "": emtpy string - do not create
            str: Automatically create object with this prefix
    Returns:
        list: list of objects names. Normally 0 or 1 element only
    """
    host_name = []
    response = client.api_call("show-hosts", {"filter": host_str})

    if response.success:
        (mds_ip, mds_username, dmn) = loggedDMN_by_client(client)
        total = response.data["total"]
        if total == 0:
            if auto_create_prefix:
                if validate_ip(host_str):
                    color = "Red"
                    comment = "created automatically"
                    prefixed_host_name = auto_create_prefix+host_str
                    response = add_host(client, prefixed_host_name, host_str,
                                        color=color, comments=comment)
                    if response.success:
                        logging.info(f"! Automatically created {prefixed_host_name} ({host_str}) in {dmn}")
                        host_name.append(prefixed_host_name)
                    else:
                        logging.warning(
                            f"Failed automatic creation of {prefixed_host_name} ({host_str}) in {dmn}")
            # else:
            #     logging.warning(
            #         f"Not Found {host_str} in {dmn} : "
            #         f"{response.data['objects']}"
            #     )
            return host_name
        if total > 1:
            logging.warning(
                f"Found {total} objects (expected 1) {host_str} in {dmn} : "
                f"{response.data['objects']}"
            )
        for object in response.data['objects']:
            host_name.append(object['name'])
    else:
        logging.warning(f"find_host(): show-host: {response.error_message}")
    return host_name # find_host


def loggedDMN_by_client(client: APIClient):
    """Returns a key for LoggedDMN

    Args:
        client (APIClient): result of api_call("login")

        tuple (mds_ip, mds_username, dmn)
    """

    for k in LoggedDMN:
        if LoggedDMN[k]["client"] == client:
            (mds_ip, mds_username, dmn) = k
            logging.debug(
                f"loggedDMN_by_client() found: {mds_ip}/{dmn} as {mds_username}")
            return (mds_ip, mds_username, dmn)
    return (None, None, None) # loggedDMN_by_client


def logout_mdm(client: APIClient):
    """logout from mdm and clear this session from cache if possible

    Args:
        client (APIClient): from login_mdm

    Returns:
        bool:
            True: logged out [and deleted from cache]
            False: logout failed
    """

    global LoggedDMN
    # clear from cache if exists
    for k in LoggedDMN:
        if LoggedDMN[k]["client"] == client:
            (mds_ip, mds_username, dmn) = k
            logging.info(f"Logout from: {mds_ip}/{dmn} as {mds_username}")
            del LoggedDMN[k]
            response = client.api_call("logout")
            if not response.success:
                logging.debug( f"{response.status_code}: '{response.error_message}'" )
                logging.info( f"{response.status_code}: '{response.error_message}'" )
            break
    return response.success # logout_mdm


def logout_mdm_cached():
    """ logout from all cached sessions and clear LoggedDMN """
    global LoggedDMN

    # clear from cache if exists
    for k in LoggedDMN:
        (mds_ip, mds_username, dmn) = k
        logging.info(f"Logout from: {mds_ip}/{dmn} as {mds_username}")
        response = LoggedDMN[k]["client"].api_call("logout")
        if not response.success:
            logging.debug( f"{response.status_code}: '{response.error_message}'" )
            logging.info( f"{response.status_code}: '{response.error_message}'" )
    LoggedDMN = {}
    return # logout_mdm_cached


def login_mdm(mds_ip, mds_username, mds_password, dmn="", ignore_fail=True, cached = True, silent = False):
    """login to MDM or DMN (if set)

    Args:
        mds_ip (str): MDS IP
        mds_username (str): username
        mds_password (str): password
        dmn (str, optional): DMN. Defaults to "" (if login to MDM itself).
        ignore_fail (bool, optional): Exit on failed login. Defaults to True.
        cached (bool, optional): Defaults to True.
            True: Use cached session or login and store this session in cache.
            False: Just login, do not alter cache.

    Returns tuple:
        APIClient: client object to use in
        APIResponse: ``login`` call response
    """
    logging.debug(f"Logging to: {mds_ip}/{dmn} as {mds_username}, cached: {cached}")

    global LoggedDMN

    # dmn_name = dmn # if not dmn else "_MDM_" # use _MDM_ as a key if dmn == ""
    if not cached:
        # login using API
        client_args = APIClientArgs(server=mds_ip, unsafe_auto_accept=True)
        client = APIClient(client_args)

        # The API client, would look for the server's certificate SHA1 fingerprint in a file.
        # If the fingerprint is not found on the file, it will ask the user if he accepts it.
        # In case the user does not accept the fingerprint, exit the program.
        # (!) Though unsafe_auto_accept=True avoids waiting for user confirmation below
        if client.check_fingerprint() is False:
            logging.critical("Could not get the server's fingerprint - Check connectivity.")
            sys.exit(1)

        for i in range(0,10): # retries if err_too_many_requests (3 logins per 1 minute allowed)
            response = client.login(mds_username, mds_password, domain = dmn)
            logging.debug(f"login_mdm:\n{response}\n")
            if response.success is True:
                break
            else:
                if response.status_code == 400:
                    logging.debug( f"i={i} err: '{response.error_message}'" )
                    break
                if response.data['code'] != "err_too_many_requests":
                    logging.debug( f"i={i} err: '{response.error_message}'" )
                    break
                else:
                    logging.warning( f"login retry {i+1}" ) # must not appear with login caching
                    sleep(10)
        if response.success is False:
            if not ignore_fail:
                logging.critical(
                    f"Login to {mds_ip}/{dmn} as {mds_username} failed: {response.status_code}\n{response}")
        else:
            if not silent:
                logging.info(f"Logged to: {mds_ip}/{dmn} as {mds_username}")
    else:
        # try cache, if not - log in
        try:
            client = LoggedDMN[(mds_ip, mds_username, dmn)]["client"]
        except KeyError:
            # logging.warning(f" not in cache: {mds_ip}/{dmn} as {mds_username}\n")
            (client, response) = login_mdm(mds_ip, mds_username, mds_password, dmn, cached=False)
            if response.success:
                LoggedDMN[(mds_ip, mds_username, dmn)] = { "client": client, "session": response.data["uid"], "changed": False }
        else:
            json_response = {
                "uid": LoggedDMN[(mds_ip, mds_username, dmn)]["session"]}
            response = APIResponse(
                json_response, success=True, status_code=200, err_message="")
            if not silent:
                logging.info(f"Logged to: {mds_ip}/{dmn} as {mds_username} [cached]")
    return (client, response) # login_mdm


def create_dmn(client, mds_name, mds_dmn, mds_dmn_ip):
    response = client.api_call("show-domain", {"name": mds_dmn})
    logging.debug(f"create_dmn: show-domain {mds_dmn}\n{response}\n")
    if response.success is False:
        # Domain not found, to be created
        logging.info(f"Creating domain {mds_dmn} {mds_dmn_ip}")
        response = client.api_call("add-domain",
            {
                "name": mds_dmn,
                "servers": {"name": f"{mds_dmn}_Server",
                        "ip-address": mds_dmn_ip,
                        "multi-domain-server": mds_name},
                # "color": "blue",
                # "details-level": "full",
            }
        )
        logging.debug(f"create_dmn: add-domain\n{response}\n")

        response = client.api_call("show-domain", {"name": mds_dmn})
        logging.debug(f"create_dmn: show-domain\n{response}\n")
        if response.success:
            logging.info(f"Domain {mds_dmn} created.")
        else:
            logging.critical(f"create_dmn: Error when creating domain:\n{response}\n")
    else:
        logging.debug(f"Domain {mds_dmn} is already present")
    return response # create_dmn


def reassign_dmn(client, dependent_domain, global_domain = "global"):
    # a delay might be required after Domain Server creation to FWM starts
    const_retry = 5
    for retry in range(0, const_retry):
        response = client.api_call("assign-global-assignment", {
                "global-domains": global_domain,
                "dependent-domains": dependent_domain,
                }
            )
        if response.success:
            break
        logging.info(retry+1)
        sleep(10)

    logging.debug(f"reassign_dmn: assign-global-asssignment\n{response}\n")
    if response.success and (response.status_code == 200):
        logging.info("... [re]assigned.")
    else:
        logging.error(
            f"Error when reassigning global assignment: {response.data['tasks'][0]['comments']}\n")
    return response # reassign_dmn


def assign_dmn(client, dependent_domain, global_domain = "global",
            global_access_policy = "standard", manage_protection_actions = True):
    response = client.api_call("show-global-assignment",
        {"global-domain": global_domain, "dependent-domain": dependent_domain})
    logging.debug(f"assign_dmn: show-global-asssignment\n{response}\n")
    if response.success:
        logging.warning(
            f"Assignment exists: {global_access_policy} of {global_domain} to {dependent_domain}")
        response = reassign_dmn(client, dependent_domain)
    else:
        # Assignment not found, to be created
        logging.info(f"Assigning {global_access_policy} of {global_domain} to {dependent_domain}")
        response = client.api_call("add-global-assignment", {
                "global-domain": global_domain,
                "dependent-domain": dependent_domain,
                "global-access-policy": global_access_policy,
                # "global-threat-prevention-policy" : "standard",
                "manage-protection-actions": manage_protection_actions,
                }
            )
        logging.debug(f"assign_dmn: add-global-asssignment\n{response}\n")
        if not response.success:
            logging.error(f"assign_dmn: add-global-assignment (NOT added)\n{response}\n")
        response = client.api_call("publish", {})
        logging.debug(f"assign_dmn: publish\n{response}\n")
        response = reassign_dmn(client, dependent_domain)
        if response.success and (response.status_code == 200):
            logging.info("Global assignment created.")
        else:
            logging.error(
                f"Error when adding global assignment: {response.data['tasks'][0]['comments']}\n")
    return response # assign_dmn


def create_network(client, net_name, subnet, subnet_mask, color = "crete blue"):
    response = client.api_call("show-network", {"name": net_name})
    logging.debug(f"create_network: show-network\n{response}\n")
    if response.success is False:
        response = client.api_call("add-network",
            {"name": net_name, "subnet": subnet, "subnet-mask": subnet_mask, "color": color})
        logging.debug(f"create_network: add-network\n{response}\n")
        if response.success is False:
            logging.error(f"Error creating network {net_name}")
            logging.error(f"\n{response}\n")
        else:
            logging.info(f"Network {net_name} created")
    else:
        # print(response)
        logging.warning(f"Network {net_name} exists")
        response = client.api_call("set-network",
            {"name": net_name, "subnet": subnet, "subnet-mask": subnet_mask, "color": color})
        logging.debug(f"create_network: set-network\n{response}\n")
    return response.success # create_network


def add_host(client, host_name, host_ip, color, ignore_warnings=False, ignore_errors=False,
        interfaces=None, nat_settings=None, tags=None, groups=None, comments=None):
    """ Add / set host """
    params = {"name": host_name, "ip-address": host_ip, "color": color,
        "ignore-warnings": ignore_warnings, "ignore-errors": ignore_errors, "set-if-exists": False }
    if color is not None:
        params["color"] = color
    else:
        params["color"] = "Crete Blue"
    if interfaces is not None:
        params["interfaces"] = interfaces
    if nat_settings is not None:
        params["nat-settings"] = nat_settings
    if tags is not None:
        params["tags"] = tags
    if groups is not None:
        params["groups"] = groups
    if comments is not None:
        params["comments"] = comments
    response = client.api_call("add-host", params)
    logging.debug(f"add_host: add-host\n{response}\n")
    return response # add_host


def set_group(client, group_name, host_name, color=None, ignore_warnings=False, ignore_errors=False,
        tags=None, comments=None, action = "add"):
    """ Set group ( add/remove host) """

    # ToDo Check host (create it by IP first ?)



    response = client.api_call("show-group", {"name": group_name})
    if not response.success:
        # Create group if missed
        response = client.api_call("add-group", {"name": group_name})
        if response.success:
            logging.info( f"set_group(): Group {group_name} was missed. Created." )
        else:
            logging.warning(f"Failed adding group. set_group: add-group\n{response.error_message}\n")
            logging.debug(f"set_group(): add-group\n{response}\n")
    else:
        # Group exists. Check if the host is already its memeber.
        logging.debug(f"set_group(): show-group\n{response}\n")
        for host in response.data['members']:
            if host['name'] == host_name:
                # Already a member
                logging.info( f"set_group(): {host_name} is already a member of {group_name}" )
                response.extra_message = "already_member"
                return response

    params = {"name": group_name, "members": {action: host_name}, "color": color,
        "ignore-warnings": ignore_warnings, "ignore-errors": ignore_errors, } # "set-if-exists": False
    if color is not None:
        params["color"] = color
    else:
        params["color"] = default_color
    if tags is not None:
        params["tags"] = tags
    if comments is not None:
        params["comments"] = comments
    response = client.api_call("set-group", params)
    logging.debug(f"set_group: set-group\n{response}\n")
    if response.success:
        logging.info( f"{host_name} added to group {group_name}." )
    else:
        logging.error(f"Failed adding {host_name} added to group {group_name}. set_group: add-group\n{response.error_message}\n")
        logging.debug(f"set_group: add-group\n{response}\n")
    return response # set_group


def create_host(client, host_name, host_ip, color = "crete blue"):
    """ Deprecated, remove """
    response = client.api_call("show-host", {"name": host_name})
    logging.debug(f"create_host: show-host\n{response}\n")
    if response.success is False:
        response = client.api_call("add-host",
            {"name": host_name, "ip-address": host_ip, "color": color})
        logging.debug(f"create_host: add-host\n{response}\n")
        if response.success is False:
            logging.error(f"Error creating host {host_name}")
            logging.error(f"\n{response}\n")
        else:
            logging.info(f"Host {host_name} created")
    else:
        logging.warning(f"Host {host_name} exists")
        response = client.api_call("set-host",
            {"name": host_name, "ip-address": host_ip, "color": color})
        logging.debug(f"create_host: set-host\n{response}\n")
    return response.success # create_host


def get_layer(client, layer_name):
    """ Return Layer uid for "local domain" and ignore "Global" """
    logging.info(f"Get UID for layer '{layer_name}'")
    response = client.api_call("show-access-layers").data['access-layers']
    logging.debug(f"get_layer: show-access-layer\n{response}\n")
    for layer in response:
        # print_note(f"Layer {layer['name']} {layer['uid']} {layer['domain']['domain-type']}\n", True)
        if (layer['domain']['domain-type'] == "domain") and (layer['name'] == layer_name):
            return layer['uid']
        logging.debug(f"Layer {layer['uid']} {layer['domain']['domain-type']}\n")
    return None # get_layer


def create_section(client, layer_uid, section_name, position):
    logging.info(f"Create section {section_name} at {position}")
    response = client.api_call("show-access-section", {"layer": layer_uid, "name": section_name})
    logging.debug(f"create_section: show-access-section\n{response}\n")
    if not response.success:
        # section not found, to be created
        response = client.api_call("add-access-section",
            {"layer": layer_uid, "name": section_name, "position": position,
            "ignore-warnings": True, "details-level": "full"})
        if response.success:
            logging.info(f"Section {section_name} created at {position}")
        logging.debug(f"create_section: add-access-section\n{response}\n")
    return response # create_section


def create_rule(client, layer_uid, name, position, src, dst, svc, action, track, comments="", enabled = True ):
    logging.info(f"create_rule() {name} at {position}")
    response = client.api_call("show-access-rule", {"layer": layer_uid, "name": name})
    logging.debug(f"create_rule: show-access-rule\n{response}\n")
    if response.status_code == 404:
        response = client.api_call("add-access-rule", {
                "layer": layer_uid, "name": name,
                "source": src, "destination": dst, "service": svc,
                "action": action, "track": track, "position": position, "enabled": enabled,
                "comments": comments, "ignore-warnings": True, "details-level": "full"}
            )
        logging.debug(f"create_rule: add-access-rule\n{response}\n")
    else:
        logging.warning(f"The rule: '{name}' already exists.")
        response = client.api_call("set-access-rule", {
                "layer": layer_uid, "name": name,
                "source": src, "destination": dst, "service": svc,
                "action": action, "track": track, "position": position, "enabled": enabled,
                "comments": comments, "ignore-warnings": True, "details-level": "full"}
            )
        logging.debug(f"create_rule: set-access-rule\n{response}\n")

    if not response.success:
        logging.error(
            f"The rule: '{name}' has NOT been added/updated: {response.error_message}")
    return response # create_rule


def main():
    pass

if __name__ == "__main__":
    main()
