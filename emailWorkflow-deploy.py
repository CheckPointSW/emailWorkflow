# pylint: disable=W1203,C0116,C0410
"""Deploy objects and rules from email"""

from cmath import log
from distutils.log import Log
import sys, os, logging, colorlog

import imaplib, extract_msg
import configparser
import json

# https://fedingo.com/how-to-send-html-mail-with-attachment-using-python/
import smtplib, ssl, email
# from email import encoders
# from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

from jinja2 import Template
from bs4 import BeautifulSoup
import keyring

import cpa #  Functions for Check Point Mgmt API
from cpa import logout_mdm_cached, print_note as print_note
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

# Global vars
cfg = None
sectEmails = "EMAILS"
sectCP = "CP"
sectCMA = "CP_CMA_IP"
LogFormat = "  %(log_color)s%(levelname)-8s%(reset)s | %(log_color)s%(message)s%(reset)s"
LogLevel = logging.INFO
# LogLevel = logging.DEBUG

MDS_ip = ""
MDS_username = ""
MDS_password = ""


def deploy311( cp_table ):
    """ Define Objects/Groups used in rules """
    cpa.logfuncname()

    table = sorted(cp_table, key=lambda d: d['CMAName']) # group by CMA to minimize context switching
    #print( table )
    try:
        for row in table:
            if row['Action'].strip().lower() != "no change":
                dmn = row['CMAName']
                (mdm_client_dmn, response) = cpa.login_mdm(
                    MDS_ip, MDS_username, MDS_password, dmn,
                )
                if not response.success:
                    logging.error(
                        f"Deploy tbl311: Skipping add object {row['ObjectName']}. "
                        f"Failed login to {dmn}: {response.error_message}\n")
                    logging.debug(f"{response}")
                else:
                    # Add hosts if logged in
                    logging.debug(f"add_object {row['ObjectName']}")
                    color = row['Color'] if len(row['Color']) > 0 else None
                    comment = row['Comment'] if len(row['Comment']) > 0 else None
                    response = cpa.add_host(mdm_client_dmn, row['ObjectName'], row['Details'],
                                        color=color, comments=comment)
                    if response.success:
                        # logging.info(
                        #     f"Added {row['ObjectName']} {row['Details']} on DMN {cma_name}"
                        # )
                        print_note(
                            f"Added {row['ObjectName']} {row['Details']} on DMN {dmn}")
                        cpa.setLoggedDMN((MDS_ip, MDS_username, dmn), "changed", True)
                    else:
                        logging.warning(
                            f"Failed adding {row['ObjectName']} {row['Details']} on DMN {dmn}: "
                            f"{response.error_message}\n"
                        )
                        logging.debug(f"{response.error_message}")
    except Exception as exception:
        print(exception)
    return # deploy311


def deploy312( cp_table ):
    """ Modify Object Membership in Group """
    cpa.logfuncname()

    table = sorted(cp_table, key=lambda d: d['CMAName']) # group by CMA to minimize context switching
    #print( table )
    try:
        for row in table:
            if row['Action'].strip != "No Change":
                dmn = row['CMAName']
                (mdm_client_dmn, response) = cpa.login_mdm(
                    MDS_ip, MDS_username, MDS_password, dmn,
                )
                if not response.success:
                    logging.error(
                        f"Deploy tbl312: Skipping group update. "
                        f"Failed login to {dmn}: {response}\n")
                    logging.debug(f"{response}")
                else:
                    # Add objects to groups
                    # use prefix "h-" to automatically create host if not found
                    host_names = cpa.find_host(mdm_client_dmn, row['Details'], "h-")
                    if len(host_names) == 0:
                        logging.warning(
                            f"Object {row['Details']} object not found on DMN {dmn}. "
                            f"Failed adding it to {row['GroupName']} \n"
                        )
                    else:
                        host_name = host_names[0] # consider only 1 element in the list
                        logging.debug( f"set-group {row['GroupName']} / {host_name}" )
                        comment = row['Comment'] if len(row['Comment']) > 0 else None
                        response = cpa.set_group( mdm_client_dmn, row['GroupName'], host_name,
                            comments=comment )
                        if response.success:
                            if not hasattr(response, "extra_message") or response.extra_message != "already_member":
                                print_note(f"Added {host_name} to {row['GroupName']} on DMN {dmn}")
                            cpa.setLoggedDMN((MDS_ip, MDS_username, dmn), "changed", True)
                        else:
                            logging.warning(
                                f"Failed adding {row['Details']} to {row['GroupName']} on DMN {dmn}: "
                                f"{response.error_message}\n"
                            )
                            logging.debug(f"{response.error_message}")
    except Exception as exception:
        print(exception)
    return # deploy312


def deploy313( cp_table ):
    """ Firewall Policies - Access Rules """
    cpa.logfuncname()

    table = sorted(cp_table, key=lambda d: d['CMAName']) # group by CMA to minimize context switching
    try:
        #print( table )
        for row in table:
            if row['Action'].strip != "No Change":
                dmn = row['CMAName']
                (mdm_client_dmn, response) = cpa.login_mdm(
                    MDS_ip, MDS_username, MDS_password, dmn,
                )
                if not response.success:
                    logging.error(
                        f"Deploy tbl313: Skipping group update. "
                        f"Failed login to {dmn}: {response}\n")
                    logging.debug(f"{response}")
                else:
                    # Find section [CP][section_name]
                    layer_name = "Network"
                    if row['pkgName'] != "Standard":
                        layer_name = row['pkgName'] + " " + layer_name
                    # There might be two "Network" layers - from Local and Global domains, need UUID of the Local
                    layer_UID = cpa.get_layer(mdm_client_dmn, layer_name)
                    # print(layer_UID)

                    # Try to find the section in the package
                    response = mdm_client_dmn.api_call("show-access-section", {
                        "layer": layer_UID,
                        "name": "Final",
                        })
                    if not response.success: # section not found
                        cpa.setLoggedDMN((MDS_ip, MDS_username, dmn), "changed", True)
                        response = cpa.create_section(mdm_client_dmn, layer_UID, "Final", 1)
                        logging.debug(f"create_section(Final): {response}")
                        if response.success:
                            print_note(
                                f"Created section 'Final' in {dmn}/{row['pkgName']}")
                        else:
                            print_note(
                                f"Can't create section 'Final' in {dmn}/{row['pkgName']}", True)

                    response = mdm_client_dmn.api_call("show-access-section", {
                        "layer": layer_UID,
                        "name": cfg[sectCP]["section_name"],
                        })

                    if not response.success: # section not found, to be created
                        if not cpa.LoggedDMN[(MDS_ip, MDS_username, dmn)]["changed"]:
                            cpa.LoggedDMN[(MDS_ip, MDS_username, dmn)]["changed"] = True
                        response = cpa.create_section(
                            mdm_client_dmn, layer_UID, cfg[sectCP]['section_name'], "top")
                        logging.debug(
                            f"create_section({cfg[sectCP]['section_name']}): {response}")
                        if response.success:
                            print_note(
                                f"Created section {cfg[sectCP]['section_name']} in {dmn}/{row['pkgName']}")
                        else:
                            print_note(
                                f"Can't create section {cfg[sectCP]['section_name']} in {dmn}/{row['pkgName']}", True)

                    # ToDo check if a List (several objects)

                    # If hosts not fount create them automatically prefixed with "h-"
                    src = cpa.find_host(mdm_client_dmn, row["SourceObjects"], "h-")
                    dst = cpa.find_host(mdm_client_dmn, row["DestinationObjects"], "h-")
                    if (len(src) < 1) or (len(dst) < 1):
                        logging.error(
                            f"Can't create rule, missed object[s] in {dmn}: "
                            f"{row['SourceObjects']}({src}) {row['DestinationObjects']}({dst}) ")
                    else:
                        response = cpa.create_rule(mdm_client_dmn,
                                layer_UID,
                                name = row['RulePosition'],
                                position={"bottom": cfg[sectCP]['section_name']},
                                src = src,
                                dst = dst,
                                svc = row["Protocols"],
                                action = row["Action"],
                                track = "Log",
                                comments = (row['CommentRuleNo'] + " " + row['SourceZone'] + \
                                    " " + row['DestinationZone']).strip(),
                            )
                        if response.success:
                            cpa.setLoggedDMN((MDS_ip, MDS_username, dmn), "changed", True)
                            print_note(
                                f"Added rule {row['SourceObjects']} {row['DestinationObjects']} "
                                f"{row['Protocols']} to {dmn}/{row['pkgName']}"
                            )
                        else:
                            logging.error(
                                f"Failed to add rule {row['SourceObjects']} {row['DestinationObjects']} "
                                f"{row['Protocols']} to {dmn}/{row['pkgName']}"
                            )
    except Exception as exception:
        print(exception)
    return # deploy313


def deploy( cp_tables ):
    """ Deploy objects, rules, etc from FWC sheet tables """
    cpa.logfuncname()
    # sys.exit()

    deploy311(cp_tables["tbl311"])
    deploy312(cp_tables["tbl312"])
    deploy313(cp_tables["tbl313"])

    logging.debug(f"Changes processed. Proceed to SmartConsole.")
    dmn_changed = ""
    for k in cpa.LoggedDMN:
        (mds_ip, mds_username, dmn) = k
        if cpa.LoggedDMN[k]["changed"]:
            response = cpa.LoggedDMN[k]["client"].api_call(
                "assign-session", {"administrator-name": cfg[sectCP]['SWITCHED_ADMIN']})
            response = cpa.LoggedDMN[k]["client"].api_call(
                "continue-session-in-smartconsole", {"uid": cpa.LoggedDMN[k]["session"]})
            if response.success:
                dmn_changed = dmn_changed + " " + dmn
                logging.info(f"Continue in SmartConsole ({dmn})")
            else:
                if response.error_message.find("without any changes"):
                    logging.info(f"No actual changes in {dmn}")
                else:
                    logging.warning(f"{response.error_message}\n")
            # res = os.system(cfg[sectCP]['SC_PATH']) # May run SmartConsole to IP with SmartConsole.LoginParams
    cpa.logout_mdm_cached()
    if len(dmn_changed) > 0:
        print_note(f"\n-=> Run SmartConsole for CMA: {dmn_changed}\n")
    return # deploy


def init():
    colorlog.basicConfig(format=LogFormat, stream=sys.stdout, level=LogLevel,
        log_colors={
            'DEBUG':    'light_black',
            'INFO':     'green',
            'WARNING':  'yellow',
            'ERROR':    'red',
            'CRITICAL': 'bold_red', # 'red,bg_white'
        }
    ) # DEBUG INFO WARNING ERROR CRITICAL

    # logging.debug("debug")
    # logging.info("info")
    # logging.warning("warning")
    # logging.error("error")
    # logging.critical("critical")

    # print_note( "YEP"+" OK" )
    # exit()

    global cfg
    cfg = configparser.ConfigParser(inline_comment_prefixes="#")
    cfg.optionxform = lambda option: option # Make case sensitive
    cfg.read("emailWorkflow.ini")

    global MDS_ip, MDS_username, MDS_password
    MDS_ip = cfg[sectCP]["MDS_IP"]
    MDS_username = os.getenv(cfg[sectCP]["MDS_ENV_USERNAME"])
    MDS_password = os.getenv(cfg[sectCP]["MDS_ENV_PASSWORD"])

    return # init


def finalize():
    pass


def send(eFrom, eTo, sender_email, receiver_email, smtpserver, smtpport, password, eSubject, eBody):
    msg = MIMEMultipart("alternative")
    msg["From"] = eFrom
    msg["To"] = eTo
    msg["Subject"] = eSubject

    part = MIMEText(eBody, "html")
    msg.attach(part)

    ## Comment to avoid sending
    context = ssl.create_default_context()
    with smtplib.SMTP_SSL(smtpserver, smtpport, context=context) as server:
        server.login(sender_email, password)
        res = server.sendmail(
            sender_email, receiver_email, msg.as_string()
        )
    print(f"email {sender_email} -> {receiver_email} sent: {res}\n")


def parseEmail( message ):
    emailbody = BeautifulSoup(message, "html.parser")
    # print( emailbody )
    emailtables = emailbody.find_all('table')
    logging.info( f"{len(emailtables)} tables found:" )
    logging.info( "=========================================================\n" )
    if len(emailtables) == 0:
        logging.warning("No tables found in email. Nothing to deploy.\n")
        return

    cp_tables = {}
    # ID=tbl311: CMA Name / Object Name / Details / Color / NAT / Valid IP Address / Zone / Action / Comment /
    tbl_headers = {}
    # ToDo Get headers from the table itself? (Though hardcoded less prone to human mistakes while editing)
    tbl_headers['tbl311'] = [ 'CMAName', 'ObjectName', 'Details',  'Color', 'NAT', 'ValidIP', 'Zone', 'Action', 'Comment' ]
    tbl_headers['tbl312'] = [ 'CMAName', 'GroupName', 'Details', 'Action', 'Comment' ]
    tbl_headers['tbl313'] = [ 'CMAName', 'pkgName', 'RulePosition', 'ServiceName', 'SourceObjects', 'SourceZone',
                             'DestinationObjects', 'DestinationZone', 'Protocols', 'Action', 'CommentRuleNo', 'Severity']
    tbl_headers['tbl314'] = ['CMAName', 'Package', 'RulePosition', 'Source', 'Destination',
                             'TranslatedSource', 'TranslatedDestination', 'Action', 'CommentRuleNo']

    for tbl_id in tbl_headers.keys():
        table = emailbody.find(id=tbl_id)
        #print( table )
        rows = table.find_all("tr")
        logging.info( f"=>\tId={table.get('id')} {len(rows)-1} rows" )
        #for c in table.find_all("th"):
        #    print(c.text.strip(), end=" /\t")
        #print()
        cp_objects = []
        for row in rows:
            data = row.find_all("td")
            cp_obj = {}
            i = 0
            for d in data:
                try:
                    # print( f"{i}: {d.text.strip()}", end=" /\t" )
                    cp_obj[ tbl_headers[tbl_id][i] ] = d.text.strip()
                except IndexError:
                    pass
                i += 1
            if ( len(cp_obj) > 0 ):
                cp_obj["CMAName"] = ''.join(c for c in cp_obj["CMAName"] if (c.isalnum() or c in "-_")) # Clean for other fields?
                if not (cp_obj["CMAName"] in ["CMA Name", "CMAName", "Objects"]):
                    cp_objects.append( cp_obj )
            #for data in r.find_all("td"):
            #    print( data.text.strip(), end="\t" )
            #print()
        #print( cp_objects )
        cp_tables[tbl_id] = cp_objects

    if LogLevel == logging.INFO:
        for tbl in cp_tables:
            logging.info( tbl )
            for row in cp_tables[tbl]:
                print(row)
            print()
    logging.debug( json.dumps(cp_tables, indent=4, sort_keys=False) )
    print()
    deploy( cp_tables ) # Deploy parsed changes
    return


def recv():
    """Receive email for cpauto@ and pass for further processing"""
    imapserver = cfg[sectEmails]['imapserver']
    imapport = cfg[sectEmails]['imapport']
    username = cfg[sectEmails]['cpauto']
    password = keyring.get_password("emailWorkflow", "cpauto_pass")
    try:
        imap = imaplib.IMAP4_SSL(imapserver, imapport) # open connection
        logging.debug(f"Connected to imap server {imapserver}:{imapport}")
        # imap.enable("UTF8=ACCEPT")
        retcode = imap.login(username, password)
        logging.debug(f"Logged to imap server: {retcode}")
    except:
        print( sys.exc_info()[1] )
        logging.critical(f"Can't login to imap server {imapserver}:{imapport}")
        sys.exit(1)

    # res = imap.list()
    retcode = imap.select('INBOX')  # ('OK', [b'2'])
    logging.debug(f"Select INBOX: {retcode}")
    if retcode[0] == 'OK':
        (retcode, messages) = imap.search(None, '(UNSEEN)') # (ALL) (UNSEEN) # ( "OK", [b'2'] )
        msgnums = messages[0].decode('utf-8').split() # ['1', '2'] for ALL; ['2'] for UNSEEN
        print_note(f"New messages: {len(msgnums)} ({msgnums})")
        logging.debug(f"Search retcode: {retcode}, messages: {msgnums}")
        for msg_id in msgnums:
            logging.debug(f"Fetching message {msg_id} of {msgnums}")
            (retcode, msg_data) = imap.fetch(msg_id, '(RFC822)')
            msg = email.message_from_bytes(msg_data[0][1], _class = email.message.EmailMessage)
            # for k in msg:
            #     print(k)
            # Return-Path, Received, Content-Type, Message-ID, Date, MIME-Version, User-Agent
            # Subject, References, Content-Language, To, From, In-Reply-To, X-Forwarded-Message-Id
            timestamp = email.utils.parsedate_tz(msg['Date'])
            (year, month, day, hour, minute, second) = timestamp[:6]
            logging.info(f"Processing {msg['Subject']} (D/M/Y H:M:S) {day}/{month}/{year} {hour}:{minute}:{second}")

            # body = msg.get_payload()
            # print(body)
            # Body details
            for part in msg.walk():
                # print(part.get_content_type())
                if part.get_content_type() == "text/html":
                    body = part.get_payload(decode=True)
                    imap.store(msg_id, '-FLAGS', '\\Seen') # Unseen - automatically on get_payload
                    parseEmail(body)
                    # and mark Seen or Deleted after processing
                    # imap.store(msg_id, '+FLAGS', '\\Seen') ## temporary comment to keep the message Unseen
                else:
                    continue

    else:
        logging.warning(f"Can't select INBOX: {retcode}")


    # imap.expunge() # after '\\Delete'
    imap.logout()
    return # recv


def recv_msg(msg_fname):
    # f = r'C:/Users/XXXXXX/Desktop/cs_auto_updated/emailWorkflow-master/Control_sheet.msg'
    msg = extract_msg.Message(msg_fname)
    parseEmail(msg.htmlBody)
    return # recv

"""
def prepareEmail():
    cfgIn = configparser.ConfigParser()
    cfgIn.read("snow_in1.ini")

    with open('snow_in1.html', mode="r", encoding="utf-8") as f:
        html_src = f.read()

    #print(html_src)
    #print("---\n")

    tbl314 = []
    for i in range (10, 13):
        i = str(i)
        item = dict( RulePosition=i, Source="1.1.1."+i, Destination="2.2.2."+i, TranslatedSource="100.100.100.1"+i, TranslatedDestination="200.200.200.2"+i, Action="Accept", CommentRuleNo="-" )
        print(item)
        tbl314.append(item)

    tm = Template(html_src)
    html = tm.render(cfgIn=cfgIn, tbl314=tbl314)

    print(html)
    send(cfg[sectEmails]['snow'], cfg[sectEmails]['fwteam'], cfg[sectEmails]['snow'], cfg[sectEmails]['fwteam'],
         cfg[sectEmails]['smtpserver'], cfg[sectEmails]['smtpport'], keyring.get_password("emailWorkflow", "snow_pass"),
         cfgIn['Section 0']['Subject'] + "123456", html)
"""

def main():
    # recv()
    recv_msg(r'./Control_sheet-2.msg')
    #prepareEmail()
    return

if __name__ == "__main__":
    # execute only if run as a script
    init()
    main()
    finalize()

