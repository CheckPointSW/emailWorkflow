# pylint: disable=W1203,C0116,C0410
"""Add host test"""

from datetime import timedelta
from datetime import datetime
import time


from cmath import log
from distutils.log import Log
import sys, os, logging, colorlog

import imaplib
import configparser
import json

# https://fedingo.com/how-to-send-html-mail-with-attachment-using-python/
import smtplib, ssl, email
# from email import encoders
# from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

from jinja2 import Template
from bs4 import BeautifulSoup
import keyring

import cpa #  Functions for Check Point Mgmt API
from cpa import print_note as print_note
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

# Global vars
cfg = None
sectEmails = "EMAILS"
sectCP = "CP"
sectCMA = "CP_CMA_IP"
LogFormat = "  %(log_color)s%(levelname)-8s%(reset)s | %(log_color)s%(message)s%(reset)s"
LogLevel = logging.INFO
# LogLevel = logging.DEBUG


def find_object(client, object_str, type="host", ip_only=True):
    """ Return object name if exists. Input may be an object name or IP """
    object_name = []
    response = client.api_call("show-objects", {"filter": object_str, "type": type, "ip-only": ip_only})
    print(response)
    """
    "data": {
        "from": 1,
        "objects": [
            {
                "color": "black",
                "domain": {
                    "domain-type": "domain",
                    "name": "Corporate",
                    "uid": "9244dfbe-e8bd-449e-9e1f-52c032cd4036"
                },
                "icon": "Objects/host",
                "ipv4-address": "1.1.1.1",
                "name": "h-2.2.2.2",
                "type": "host",
                "uid": "3541f698-655d-49e3-b5e2-15bcbd668b6f"
            }
        ],
        "to": 1,
        "total": 1
    },
    """
    if response.success:
        total = response.data["total"]
        if total != 1:
            logging.warning(f"Found {total} objects (expected 1) for {object_str}: "
                            f"{response.data['objects']}")
        for object in response.data['objects']:
            object_name.append(object['name'])
    else:
        logging.warning(f"show-host: {response.error_message}")
    logging.info(f"Searching for {object_str}. Found: {object_name}")
    # if validate_ip(object_str):
    #     # search for object with this IP
    #     logging.critical(f"Looking for object with IP {object_str}")
    #     print(response)
    #     pass
    # else:
    #     # search for object with this name
    #     logging.critical(f"Looking for object {object_str}")
    #     print(response)
    #     pass
    return object_name  # find_object


def init():
    colorlog.basicConfig(format=LogFormat, stream=sys.stdout, level=LogLevel,
        log_colors={
            'DEBUG':    'light_black',
            'INFO':     'green',
            'WARNING':  'yellow',
            'ERROR':    'red',
            'CRITICAL': 'bold_red', # 'red,bg_white'
        }
    ) # DEBUG INFO WARNING ERROR CRITICAL

    global cfg
    cfg = configparser.ConfigParser(inline_comment_prefixes="#")
    cfg.optionxform = lambda option: option # Make case sensitive
    cfg.read("emailWorkflow.ini")
    return


def main():

    (mdm_client_dmn, res) = cpa.login_mdm(
        cfg[sectCP]["MDS_IP"],
        os.getenv( cfg[sectCP]["MDS_ENV_USERNAME"] ),
        os.getenv( cfg[sectCP]["MDS_ENV_PASSWORD"] ),
        "Corporate", ignore_fail=True,
    )

    # print(find_object(mdm_client_dmn, "1.1.1.1"))

    IPstart = "1.2."

    for i in range(11, 19):
        print(f"\n\t{IPstart + str(i)}")
        for j in range(1,251):
            host_ip = IPstart + str(i) + "." + str(j)
            params = {"name": "h"+host_ip, "ip-address": host_ip, "color": "coral", "groups": "gAnsibleTest",
                    "ignore-warnings": False, "ignore-errors": False, "set-if-exists": True}
            response = mdm_client_dmn.api_call("add-host", params)
            # print(response)
            print(j, end=" ")
        print()

    mdm_client_dmn.api_call("logout", {})

    return


if __name__ == "__main__":
    # execute only if run as a script
    d1 = datetime.now()
    # time.sleep(3)
    init()
    main()
    d2 = datetime.now()
    delta = d2 - d1
    seconds = delta.total_seconds()
    print(f"\nTotal time: {seconds} sec")
    # finalize()
