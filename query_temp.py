"""Temporary resolver to find CMA by host"""

q_zone_by_host = [
    # IP Address	Env 		Zone
    ["1.1.1.1", "2G", ["App","Web"]],
    ["2.2.2.2", "Internet", ["App"]],
    ["3.3.3.3", "Corporate", ["Web"]],
    ["4.4.4.4", "Extranet", ["Pub"]],
]

q_cma_by_zone = [
    # Env  CMA
    ["2G", ["Internet-001","Internet-002","Internet-003"]],
    ["Internet", ["Internet-001","Management"]],
    ["Corporate", ["Internet-002","Internet-003"]],
    ["Extranet", ["Internet-003"]],
]

q_gw_by_host = [
    # IP Address	Zone
    ["10.10.10.100", {"Zone": "App", "CMA":
        ["Internet-001", "Internet-002"], "GW": ["gwI1a", "gwI2w"]}],
    ["20.20.20.100", { "Zone": "Web", "CMA": ["Internet-002"], "GW": ["gwI2w"]}],
    ["30.30.30.100", { "Zone": "DMZ", "CMA": ["Corporate"], "GW": ["gwCd"]}],
    ["40.40.40.100", { "Zone": "DMZ", "CMA": ["Extranet"], "GW": ["gwEd"]}],
    ["50.50.50.100", { "Zone": "Web", "CMA": ["Internet-003"], "GW": ["gwI3w"]}],
    ["60.60.60.100", { "Zone": "App", "CMA": ["Management"], "GW": ["gwMa"]}],
]


"""
Objects Name	Objects IP 	CMA Names 	Zone
v-server1	10.10.10.100	Internet-001	App
v-server2	20.20.20.100	Internet-002	Web
v-server3	30.30.30.100	Corporate	    DMZ
v-server4	40.40.40.100	Extranet 	    DMZ
v-server5	50.50.50.100	Internet-003	Web
v-server6	60.60.60.100	Management	    App
"""

prepare_gateways = [
    {"CMA": "Internet-001", "name": "gwI1a", "ip-address": "10.10.10.1", "color": "sienna"},
    {"CMA": "Internet-002", "name": "gwI2w", "ip-address": "20.20.20.1", "color": "sienna"},
    {"CMA": "Corporate", "name": "gwCd", "ip-address": "30.30.30.1", "color": "sienna" },
    {"CMA": "Extranet", "name": "gwEd", "ip-address": "40.40.40.1", "color": "sienna"},
    {"CMA": "Internet-003", "name": "gwI3w", "ip-address": "50.50.50.1", "color": "sienna" },
    {"CMA": "Management", "name": "gwMa", "ip-address": "60.60.60.1", "color": "sienna"},
#    {"CMA": "", "name": "", "ip-address": "", "color": "sienna" },
]

prepare_packages = [
    {"CMA": "Internet-001", "name": "pkgI1a", "installation-targets": "gwI1a",
        "access": True, "color": "sienna"},
    {"CMA": "Internet-002", "name": "pkgI2w", "installation-targets": "gwI2w",
        "access": True, "color": "sienna"},
    {"CMA": "Corporate", "name": "pkgCd", "installation-targets": "gwCd",
        "access": True, "color": "sienna"},
    {"CMA": "Extranet", "name": "pkgEd", "installation-targets": "gwEd",
        "access": True, "color": "sienna"},
    {"CMA": "Internet-003", "name": "I3w", "installation-targets": "gwI3w",
        "access": True, "color": "sienna"},
    {"CMA": "Management", "name": "pkgMa", "installation-targets": "gwMa",
        "access": True, "color": "sienna"},
#    {"CMA": "", "name": "", "installation-targets": "", "access": True, "color": "sienna"},
]

def find_cma_gw_by_srcdst(src, dst=""):
    """ stub to replace with SkyBox API """
    cma = []
    gateway = []
    for k,v in q_gw_by_host:
        if (k == src.strip()) or (k == dst.strip()):
            for dmn in v["CMA"]:
                cma.append(dmn)
            for gw in v["GW"]:
                gateway.append(gw)
    if len(cma) == 0:
        cma = ["-?-"]
    if len(gateway) == 0:
        gateway = ["-?-"]
    return (cma, gateway) # find_cma_gw_by_srcdst

def find_zone_by_ip(ip):
    """ stub to replace with SkyBox API """
    zone = ""
    for k,v in q_gw_by_host:
        if k == ip:
            zone = v["Zone"]
    if len(zone) == 0:
        zone = "-?-"
    return zone # find_zone_by_ip