# pylint: disable=W1203,C0116,C0410
"""Fill objects and rules to email from user imput"""

import sys, os, logging, colorlog, inspect

# https://fedingo.com/how-to-send-html-mail-with-attachment-using-python/
import smtplib, ssl, email
# from email import encoders
# from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

import poplib
import configparser
import keyring

from jinja2 import Template
from numpy import True_

import cpa #  Functions for Check Point Mgmt API

# from query_temp import q_zone_by_host as q_zone_by_host
# from query_temp import q_cma_by_zone as q_cma_by_zone
from query_temp import find_cma_gw_by_srcdst as find_cma_gw_by_srcdst
from query_temp import find_zone_by_ip as find_zone_by_ip

sectEmails = "EMAILS"
sectCP = "CP"
sectCMA = "CP_CMA_IP"
LogFormat = "  %(log_color)s%(levelname)-8s%(reset)s | %(log_color)s%(message)s%(reset)s"
LogLevel = logging.INFO
# LogLevel = logging.DEBUG

def init():
    colorlog.basicConfig(format=LogFormat, stream=sys.stdout, level=LogLevel,
        log_colors={
            'DEBUG':    'light_black',
            'INFO':     'green',
            'WARNING':  'yellow',
            'ERROR':    'red',
            'CRITICAL': 'bold_red', # 'red,bg_white'
        }
    ) # DEBUG INFO WARNING ERROR CRITICAL

    global cfg
    cfg = configparser.ConfigParser(inline_comment_prefixes="#")
    cfg.optionxform = lambda option: option # Make case sensitive
    cfg.read("emailWorkflow.ini")

    # CMAs = {k:v for k, v in cfg[sectCMA].items()}
    # print(CMAs)


def finalize():
    pass


def prepareMDM():
    """ Initial test deployment """
    cpa.logfuncname()
    mdm_client_dmn = cpa.login_mdm(
        cfg[sectCP]["MDS_IP"],
        os.getenv( cfg[sectCP]["MDS_ENV_USERNAME"] ),
        os.getenv( cfg[sectCP]["MDS_ENV_PASSWORD"] ),
    )

    for dmn_name, dmn_ip in cfg[sectCMA].items():
        print( f"CMA: {dmn_name} {dmn_ip}")
        res = cpa.create_dmn(mdm_client_dmn, cfg[sectCP]["MDS_NAME"], dmn_name, dmn_ip)

    #res = cpa.create_dmn(mdm_client_dmn, cfg[sectCP]["MDS_NAME"], "Internet-001", "172.23.23.166")
    response = cpa.logout_mdm(mdm_client_dmn)
    return # prepareMDM


def send(eFrom, eTo, sender_email, receiver_email, smtpserver, smtpport, password, eSubject, eBody):

    msg = MIMEMultipart("alternative")
    msg["From"] = eFrom
    msg["To"] = eTo
    msg["Subject"] = eSubject

    part = MIMEText(eBody, "html")
    msg.attach(part)

    #with open(filename, "rb") as attachment:
    #part = MIMEBase("application", "octet-stream")
    #part.set_payload(attachment.read())
    #encoders.encode_base64(part)
    #part.add_header(
    #"Content-Disposition",
    #"attachment", filename= filename
    #)
    #msg.attach(part)

    ## Comment to avoid sending
    context = ssl.create_default_context()
    with smtplib.SMTP_SSL(smtpserver, smtpport, context=context) as server:
        server.login(sender_email, password)
        res = server.sendmail(
            sender_email, receiver_email, msg.as_string()
        )
    print(f"email {sender_email} -> {receiver_email} sent: {res}\n")

def recv():
    pop3server = cfg[sectEmails]['popserver']
    pop3port = cfg[sectEmails]['popport']
    username = cfg[sectEmails]['fwteam']
    password = keyring.get_password("emailWorkflow", "fwteam_pass")
    pop3server = poplib.POP3_SSL(pop3server, pop3port) # open connection
    print (pop3server.getwelcome()) #show welcome message
    pop3server.user(username)
    pop3server.pass_(password)
    pop3info = pop3server.stat() #access mailbox status
    mailcount = pop3info[0] #toral email
    print("Total no. of Email : " , mailcount)
    print ("\n\nStart Reading Messages\n\n")
    #for i in range(mailcount):
    #    for message in pop3server.retr(i+1)[1]:
    #        print (message)
    pop3server.quit()
    return


PKG_GW_packages = {} # for cache

def show_pkg_gw(dmn, gw):
    cpa.logfuncname( f"{dmn},{gw}" )

    # cpa.print_note(f"\tshow_pkg_gw({dmn},{gw}):", True )

    # global PKG_GW_packages
    # try:
    #     packages = PKG_GW_packages[dmn][gw]
    #     return packages
    # except:
    #     pass

    packages = []

    (mdm_client_dmn, response) = cpa.login_mdm(
        cfg[sectCP]["MDS_IP"],
        os.getenv(cfg[sectCP]["MDS_ENV_USERNAME"]),
        os.getenv(cfg[sectCP]["MDS_ENV_PASSWORD"]),
        dmn=dmn, silent=True_,
    )
    if not response.success:
        logging.error(
            f"show_pkg_gw({dmn}, {gw}): "
            f"Failed login to {dmn}: {response.error_message}\n")
        logging.debug(f"{response}")
    else:
        response = mdm_client_dmn.api_call("show-packages")
        logging.debug(f"{dmn} {gw}: show-packages \n{response}\n")
        if response.success:
            for pkg in response.data["packages"]:
                pkg_name = pkg["name"]
                # ? need suffix to ignore backup copies ?
                pkg_uid = pkg["uid"]
                logging.debug(f"{pkg_name} {pkg_uid}")
                response = mdm_client_dmn.api_call(
                    "show-package", {"uid": pkg_uid})
                if response.success and (type(response.data['installation-targets']) is not str):
                    targets = response.data['installation-targets']
                    for target in targets:
                        if target['name'] == gw:
                            packages.append(pkg_name)
                else:
                    # ? "all" - need to include ?
                    packages.append(pkg_name)

        logging.info(f"{inspect.stack()[0][3]}(): {dmn}/{gw} in {packages}")
    return packages  # show_pkg_gw


def table_remove_duplicates(tbl):
    tbl_unique = []
    for v in tbl:
       if v not in tbl_unique:
          tbl_unique.append(v)
    return tbl_unique # table_remove_duplicates


def ObjectName(ip, dmn):
    """ Check if IP exists in CMA. Return its name/status: found / Exists or generated / New ) """
    (mdm_client_dmn, response) = cpa.login_mdm(
        cfg[sectCP]["MDS_IP"],
        os.getenv(cfg[sectCP]["MDS_ENV_USERNAME"]),
        os.getenv(cfg[sectCP]["MDS_ENV_PASSWORD"]),
        dmn=dmn, silent=True,
    )

    if response.success:
        found_name = cpa.find_host(mdm_client_dmn, ip)
    else:
        found_name = []
        cpa.print_note(f"ObjectName(): not logged to {dmn}", True)
    if len(found_name) == 0 :
        name = "s-" + ip
        status = "New"
        color = "Blue"
    else:
        name = found_name[0]
        cpa.print_note(f"{ip} found as {name} in {dmn}")
        status = "No change"
        color = "Black"

    # Blue = New Objects
    # Green = Amended Objects
    # Red = Deleted Objects
    # Black = Objects that are not be changed
    return (name, status, color) # ObjectName


def prepareEmail():
    cpa.logfuncname()
    cfgIn = configparser.ConfigParser()
    cfgIn.read("snow_in1.ini")

    with open('snow_in1.html', mode="r", encoding="utf-8") as f:
        html_src = f.read()

    #print(html_src)
    #print("---\n")

    tbl311 = [] # Define Objects/Groups used in rules
    for (rule_name, rule_val) in cfgIn.items("Section 3"):
        # print( f"{rule_name}: {rule_val}" )
        #[x for x in re.split(',| ', your_string) if x != '']
        (src, dst, svc, action, comment) = [x.strip() for x in rule_val.split(';')] # split and remove separators and spaces
        object_IPs = [src, dst]
        for ip in object_IPs:
            (cmas, gateways) = find_cma_gw_by_srcdst(ip)
            for cma in cmas:
                (object_name, action, color) = ObjectName(ip, cma)
                zone = find_zone_by_ip(ip)
                item = dict(CMAName=cma, ObjectName=object_name, Details=ip, Color=color,
                            NAT="False", ValidIP="", Zone=zone,
                            Action=action, Comment="",
                            )
                tbl311.append(item)

    tbl311 = table_remove_duplicates(tbl311)
    print( "\n TBL311")
    for r in tbl311:
        print( r )


    tbl312 = []  # Modify Object Membership in Group


    tbl313 = [] # Firewall Policies - Access Rules
    for (rule_name, rule_val) in cfgIn.items("Section 3"):
        # print( f"{rule_name}: {rule_val}" )
        #[x for x in re.split(',| ', your_string) if x != '']
        (src, dst, svc, action, comment) = [x.strip() for x in rule_val.split(';')] # split and remove separators and spaces
        (cmas, gateways) = find_cma_gw_by_srcdst(src, dst)
        cmas = table_remove_duplicates(cmas)
        gateways = table_remove_duplicates(gateways)
        for cma in cmas:
            # print( f"CMA: {cma}" )
            for gateway in gateways:
                packages = show_pkg_gw(cma, gateway)
                for package in packages:
                    src_zone = find_zone_by_ip(src)
                    dst_zone = find_zone_by_ip(dst)
                    print(cma, package, src, src_zone, dst, dst_zone, svc, action, comment, gateway, sep=" / ")
                    item = dict(CMAName=cma, pkgName=package, RulePosition=rule_name,
                        ServiceName="", SourceObjects=src, SourceZone=src_zone,
                        DestinationObjects=dst, DestinationZone=dst_zone,
                        Protocols=svc, Action=action, CommentRuleNo=comment,
                        )
                    tbl313.append(item)
    tbl313 = table_remove_duplicates(tbl313)
    print( "\n TBL313")
    for r in tbl313:
        print( r )

    tbl314 = []  # Firewall Translation Rules
    # for i in range (10, 13):
    #     i = str(i)
    #     item = dict( RulePosition=i, Source="1.1.1."+i, Destination="2.2.2."+i,
    #               TranslatedSource="100.100.100.1"+i, TranslatedDestination="200.200.200.2"+i,
    #               Action="Accept", CommentRuleNo="-" )
    #     # print(item)
    #     tbl314.append(item)

    tbl314 = table_remove_duplicates(tbl314)
    print( "\n TBL314")
    for r in tbl314:
        print( r )

    tm = Template(html_src)
    html = tm.render(cfgIn=cfgIn, tbl311=tbl311, tbl312=tbl312, tbl313=tbl313, tbl314=tbl314)

    # print(html)
    return # comment out to send email
    send(cfg[sectEmails]['snow'], cfg[sectEmails]['fwteam'], cfg[sectEmails]['snow'],
        cfg[sectEmails]['fwteam'], cfg[sectEmails]['smtpserver'], cfg[sectEmails]['smtpport'],
        keyring.get_password("emailWorkflow", "snow_pass"),
        cfgIn['Section 0']['Subject'] + cfgIn['Section 2']['ChangeTicket'], html)
    return # prepareEmail


def main():
    # recv()
    # prepareMDM()
    prepareEmail()
    return # main

if __name__ == "__main__":
    # execute only if run as a script
    init()
    main()
    finalize()

